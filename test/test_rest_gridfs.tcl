package require snit
package require TclCurl
package require json

snit::type ProgressText {
    variable startTime ""
    variable last 0
    variable lastTime 0
    
    option -total -default 100
    option -printcmd -default "puts -nonewline"
    option -eprintcmd -default "puts"
    option -flushcmd -default "flush stdout"
    
    constructor {args} {
	$self configurelist $args
    }

    method start {} {
	set startTime [clock seconds]
	set last 0
	set lastTime 0
    }

    method advance { cur } {
	set now [clock seconds]
	set tot $options(-total)

	if {$cur > $tot} {
	    set cur $tot
	}
	if {($cur >= $tot && $last < $cur) ||
	    ($cur - $last) >= (0.05 * $tot) ||
	    ($now - $lastTime) >= 5} {
	    set last $cur
	    set lastTime $now
	    set percentage [expr round($cur*100/$tot)]
	    set ticks [expr $percentage/2]
	    if {$cur == 0} {
		set eta   ETA:[format %7s Unknown]
	    } elseif {$cur >= $tot} {
		set eta   TOT:[format %7d [expr int($now - $startTime)]]s
	    } else {
		set eta   ETA:[format %7d [expr int(($tot - $cur) * ($now - $startTime)/$cur)]]s
	    }
	    set lticks [expr 50 - $ticks]
	    set str "[format %3d $percentage]%|[string repeat = $ticks]"
	    append str "[string repeat . $lticks]|[format %8d [expr round($cur)]]|$eta\r"
	    {*}$options(-printcmd) $str
	    if {$cur >= $tot} {
		{*}$options(-eprintcmd) ""
	    }
	    if {$options(-flushcmd) ne ""} {
		{*}$options(-flushcmd)
	    }
	}
    }
}

snit::type MongoBP {
    variable responseHeader -array {}
    variable bufferError
    variable bufferBody
    variable downloadFileHandler
    
    variable progress
    option -host -readonly true -default localhost
    option -port -readonly true -default 8080
    delegate option -printcmd to progress
    delegate option -eprintcmd to progress
    delegate option -flushcmd to progress
    
    constructor { args } {
	install progress using ProgressText %AUTO%
	$self configurelist $args
    }

    method clearRequest { } {
	array unset responseHeader
	set bufferError ""
    }

    #dltotal dlnow ultotal ulnow
    method cbProgressUpload {dltotal dlnow ultotal ulnow} {
	#flush stdout
	#return
	if {$ultotal == 0} {
	    return
	}
	if {[$progress cget -total] == 0} {
	    $progress configure -total $ultotal
	    $progress start 
	}
	$progress advance $ulnow
    }
    
    method cbProgressDownload {dltotal dlnow ultotal ulnow} {
	#flush stdout
	#return
	if {$dltotal == 0} {
	    return
	}
	if {[$progress cget -total] == 0} {
	    $progress configure -total $dltotal
	    $progress start 
	}
	$progress advance $dlnow
    }
    
    method uploadFile { project path args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set verbose $meta(-verbose)
	array unset meta "-verbose"
	set filename [file tail $path]
	set listProps [list [string map [list "%f" $filename] {"filename":"%f"}]]
	foreach k [array names meta] {
	    set _k [string range $k 1 end]
	    if {$_k ne ""} {
		lappend listProps "\"${_k}\":\"$meta($k)\""
	    }
	}
	set jsonProps "{[join $listProps ,]}"
	set curlHandle [curl::init]
	$progress configure -total 0
	fconfigure $outFile -translation binary
	$curlHandle configure -url http://$options(-host)/${project}/QFS.files \
	    -port $options(-port) \
	    -verbose $verbose \
	    -noprogress 0 \
	    -progressproc [mymethod cbProgressUpload] \
	    -errorbuffer [myvar bufferError] \
	    -username a \
	    -password a \
	    -headervar [myvar responseHeader] \
	    -post 1 \
	    -httppost \
	      [list name file file $path contenttype application/octet-stream filename $filename] \
	    -httppost [list name "properties" contents $jsonProps]
	set status [$curlHandle perform]
	if {$status} {
	    set result [list $status $bufferError]
	} else {
	    set location $responseHeader(Location)
	    
	    set result [list 0 [lindex [split $responseHeader(Location) /] end]]
	}
	$curlHandle cleanup
	return $result
    }

    method downloadFileActual { project id fileName args } {
	array set meta {
	    -verbose 0
	    -dest "."
	}
	array set meta $args
	set verbose $meta(-verbose)
	array unset meta "-verbose"
	set pathFile [file join $meta(-dest) $fileName]
	set curlHandle [curl::init]
	$progress configure -total 0
	$curlHandle configure -url http://$options(-host)/${project}/QFS.files/${id}/binary \
	    -port $options(-port) \
	    -verbose $verbose \
	    -file $pathFile \
	    -noprogress 0 \
	    -progressproc [mymethod cbProgressDownload] \
	    -errorbuffer [myvar bufferError] \
	    -username a \
	    -password a
	{*}[$self cget -eprintcmd] "Downloading file for id = $id to $pathFile"
	set downloadFileHandler [open  $pathFile wb]
	set status [$curlHandle perform]
	if {$status} {
	    set result [list $status $bufferError]
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    #puts "httpCode = $httpCode"
	    if {$httpCode == 200} {
		set result [list 0 $pathFile]
	    } else {
		set result [list $httpCode "http error"]
	    }
	}
	$curlHandle cleanup
	close $downloadFileHandler
	if {[lindex $result 0]} {
	    file delete $pathFile
	}
	return $result
    }
    
    method downloadFile { project id args } {
	set info [$self getFileInfo $project $id {*}$args]
	if {[lindex $info 0] == 0} {
	    return [$self downloadFileActual $project $id [dict get [lindex $info 1] filename] {*}$args]
	} else {
	    return $info
	}
    }
    
    method getFileInfo { project id args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set verbose $meta(-verbose)
	array unset meta "-verbose"
	set curlHandle [curl::init]
	$progress configure -total 0
	$curlHandle configure -url http://$options(-host)/${project}/QFS.files/${id} \
	    -port $options(-port) \
	    -verbose $verbose \
	    -noprogress 0 \
	    -progressproc [mymethod cbProgressDownload] \
	    -errorbuffer [myvar bufferError] \
	    -username a \
	    -password a \
	    -bodyvar [myvar bufferBody]
	{*}[$self cget -eprintcmd] "Downloading file metadata for id = $id"
	set status [$curlHandle perform]
	if {$status} {
	    set result [list $status $bufferError]
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    set getResult [json::json2dict $bufferBody]
	    if {$httpCode == 200} {
		set result [list 0 [dict get [json::json2dict $bufferBody] metadata]]
	    } else {
		set result [list $httpCode [dict get $getResult message]]
	    }
	}
	$curlHandle cleanup
	return $result
    }

    method deleteFile { project id args } {
	$self clearRequest
	array set meta {
	    -verbose 0
	}
	array set meta $args
	set verbose $meta(-verbose)
	array unset meta "-verbose"
	set curlHandle [curl::init]
	$curlHandle configure -url http://$options(-host)/${project}/QFS.files/${id} \
	    -port $options(-port) \
	    -verbose $verbose \
	    -customrequest "DELETE" \
	    -errorbuffer [myvar bufferError] \
	    -username a \
	    -password a \
	    -bodyvar [myvar bufferBody] \
	    -headervar [myvar responseHeader]
	set status [$curlHandle perform]
	if {$status} {
	    set result [list $status $bufferError]
	} else {
	    set httpCode [$curlHandle getinfo httpcode]
	    switch -- $httpCode {
		204 {
		    set result [list 0 "File Deleted"]
		}
		404 {
		    set result [list 204 "File Not Found"]
		}
		default {
		    set result [list $httpCode [array get responseHeader]]
		}
		
	    }
	}
	$curlHandle cleanup
	return $result
    }    
}

set mongo [MongoBP %AUTO% -host 52.50.99.11 -port 9080]

if {0} {
set progress [ProgressText %AUTO%]
$progress start 5000

for {set i 0} {$i < 6200} {incr i 200} {
   $progress advance $i
   after 200
}
}

#puts [$mongo uploadFile cokindmvaind CCI_MainBrands_CBO_FBO_013__1.0.oza -user jsperez -verbose 0]

#puts [$mongo downloadFile cokindmvaind 58bf3bfe14bafd14eadc8635 -verbose 0 -dest /tmp]
puts [$mongo deleteFile cokindmvaind 58bf26a514bafd14eadc85cc -verbose 0]

exit

#curl -v -u a:a -X POST -F 'properties={"user":"jsperez", "filename":"CCI_MainBrands_CBO_FBO_013__1.0.oza"}' -F "file=@/tmp/CCI_MainBrands_CBO_FBO_013__1.0.oza" 52.50.99.11:9080/cokindmvaind/QFS.files

set curlHandle [curl::init]
$curlHandle configure -url http://52.50.99.11/cokindmvaind/QFS.files \
    -port 9080 \
    -verbose 1 \
    -username a \
    -password a \
    -headervar resHeaders \
    -post 1 \
    -httppost \
    [list name file file "CCI_MainBrands_CBO_FBO_013__1.0.oza" contenttype application/octet-stream filename "CCI_MainBrands_CBO_FBO_013__1.0.oza"] \
    -httppost [list name "properties" contents {{"user":"jsperez", "filename":"CCI_MainBrands_CBO_FBO_013__1.0.oza"}}]
set status [$curlHandle perform]
$curlHandle cleanup

#curl::transfer -url a:a@52.50.99.11:9080/cokindmvaind/QFS.files -verbose 1 -post 1 -httppost [list contenttype application/octet-stream filename "CI_MainBrands_CBO_FBO_013__1.0.oza"]

parray resHeaders
