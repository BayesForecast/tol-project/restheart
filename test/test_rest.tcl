package require rest
package require base64

set MongoREST(getFileInfo) {
    url http://52.50.99.11:9080/cokindmvaind/QFS.files
    method get
    result json
    auth {a a}
}

proc geturl_auth {url username password} {
    set auth "Basic [base64::encode $username:$password]"
    set headerl [list Authorization $auth]
    set tok [http::geturl $url -headers $headerl]
    set res [http::data $tok]
    http::cleanup $tok
    return $res
}

rest::create_interface MongoREST

#MongoREST::getFileInfo
#puts [geturl_auth http://52.50.99.11:9080/cokindmvaind/QFS.files a a]
